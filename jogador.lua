local Jogador = {}
Jogador.__index = Jogador
function Jogador.criar(x, y, l, a, up, down)
  return setmetatable({
    x = x,
    y = y,
    l = l,
    a = a,
    h = 60,
    t = 10,
    up = up,
    down = down,
    vel = 300
  }, Jogador)
end

function Jogador:update(dt)
  if love.keyboard.isDown(self.down) and self.y <= self.a - self.h then
    self.y = self.y + (dt * self.vel)
  end
  if love.keyboard.isDown(self.up) and self.y >= 0 then
    self.y = self.y - (dt * self.vel)
  end
end

function Jogador:draw()
  love.graphics.rectangle('fill', self.x, self.y, self.t, self.h)
end
return Jogador
