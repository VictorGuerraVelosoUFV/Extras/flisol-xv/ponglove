local Bola = {}
Bola.__index = Bola
function Bola.criar(x, y, l, a, vel)
  return setmetatable({
    x = x,
    y = y,
    radius = 5,
    l = l,
    a = a,
    vel_x = vel,
    vel_y = vel,
  }, Bola)
end

function Bola:update(dt, placar, jogador1, jogador2)
  if self.y <= 0 or self.y >= self.a then
    self.vel_y = - self.vel_y
  end
  if self.x <= jogador1.t then
    if self.y >= jogador1.y  and  self.y <= jogador1.y + jogador1.h then
      self.vel_x = - self.vel_x
    else
      placar:update(2)
      self.x = self.l/2
    end
  elseif self.x >= self.l - jogador2.t then
    if self.y >= jogador2.y and  self.y <= jogador2.y + jogador2.h then
      self.vel_x = - self.vel_x
    else
      placar:update(1)
      self.x = self.l/2
    end
  end
  self.x = self.x + (self.vel_x*dt)
  self.y = self.y + (self.vel_y*dt)
end

function Bola:draw()
  love.graphics.circle('fill', self.x, self.y, self.radius, 4)
end
return Bola
