local fundo = require "fundo"
local Jogador = require "jogador"
local Placar = require "placar"
local Bola = require "bola"
local socket = require "socket"
local jogador1
local jogador2
local placar
local bola
local udp_client
local udp_server
function love.conf (t)
  t.console = true
end
function love.load()
  if arg[2] and arg[3] then
    print("arg0"..arg[2])
    print("arg1"..arg[3])
  end
  love.window.setTitle("Multiplayer Pong!")
  l, a = love.graphics.getDimensions()
  placar = Placar.criar(l, a)
  jogador1 = Jogador.criar(0,a/2, l, a, "up","down")
  jogador2 = Jogador.criar(l-15,a/2, l, a, "w","s")
  bola = Bola.criar(a/2, l/2, l, a, -200)
  if arg[2] == "27015" then
    love.window.setPosition(0, 0, 1)
  else
    love.window.setPosition(1000, 0, 1)
  end
  udp_client = socket.udp()
  udp_server = socket.udp()
  udp_server:settimeout(0)
  udp_server:setsockname("*",tonumber(arg[2]))
  udp_client:settimeout(0)
  udp_client:setpeername("127.0.0.1",tonumber(arg[3]))
end
function love.update(dt)
  jogador1:update(dt)
  jogador2:update(dt)
  bola:update(dt,placar,jogador1,jogador2)
  data, msg_or_ip, port_or_nil = udp_server:receivefrom()
  if port_or_nil then
    if arg[2] == "27015" then
      jogador2.y = tonumber(data)
    else
      jogador1.y = tonumber(data)
    end
  end
  if arg[2] == "27015" then
    udp_client:send(string.format("%d",jogador1.y))
  else
    udp_client:send(string.format("%d",jogador2.y))
  end
end
function love.draw()
  fundo()
  placar:draw()
  bola:draw()
  jogador1:draw()
  jogador2:draw()
end
