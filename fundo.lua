function desenharFundo()
  local l, a = love.graphics.getDimensions()
  for i = 15, a, 60 do
    love.graphics.rectangle('fill', l/2, i, 10, 30)
  end
end

return desenharFundo
