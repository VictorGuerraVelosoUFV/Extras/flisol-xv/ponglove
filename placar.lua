local Placar = {}
Placar.__index = Placar

function Placar.criar(l,a)
  local font = love.graphics.newFont("NotoSansSinhala-Regular.ttf", 40)
  return setmetatable({
    l = l,
    a = a,
    font = font,
    text = love.graphics.newText(font, "0 0"),
    jogador1 = {
      pontos = 0
    },
    jogador2 = {
      pontos = 0
    }
  }, Placar)
end

function Placar:update (jogador)
  if jogador == 1 then
    self.jogador1.pontos = self.jogador1.pontos + 1
  elseif jogador == 2 then
    self.jogador2.pontos = self.jogador2.pontos + 1
  end
  self.text:set(self.jogador1.pontos.." "..self.jogador2.pontos)
end

function Placar:draw ()
  love.graphics.draw(self.text, l/2 - 28, 0, 0)
end

return Placar
